package br.csi.avaliadordeproejtos.model.aluno;

public interface AlunoDTO {
    Long getId();
    String getNome();
    String getMatricula();
    String getEmail();
}

