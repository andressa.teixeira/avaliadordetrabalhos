package br.csi.avaliadordeproejtos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvaliadorDeProejtosApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvaliadorDeProejtosApplication.class, args);
    }

}
